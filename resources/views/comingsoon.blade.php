<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SYC Marketing Digital</title>
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/toastr.min.js')}}"></script>
    {!! RecaptchaV3::initJs() !!}
  </head>
  <body>
    <div class="d-md-flex h-md-100 align-items-center">
        <!-- First Half -->
        <div class="col-md-6 p-0 bg-indigo h-md-100">
            <div class="text-white d-md-flex align-items-center h-100 p-5 text-center justify-content-center">
                <div class="logoarea pt-5 pb-5">
                  <img class="logofree" src="img/white_logo_transparent_background.png" alt="Logo blanco sycmarketingdigital">
                  <p>Nuestro sitio web está en construcción</p>
                  <h1 class="text-center">PRÓXIMAMENTE</h1>
                  <p id="launch"></p>
                  @if ($message = Session::get('action'))
                    <script type="text/javascript">
                          toastr.success('Te has suscrito exitosamente!', 'Felicidades',{
                            "closeButton": true,
                            "positionClass": "toast-top-left"
                          });
                    </script>
                  @endif
                  @if ($errors->any())
                  <script type="text/javascript">
                  @foreach ($errors->all() as $error)
                     toastr.error('{{$error }}', 'Aviso',{
                       "closeButton": true,
                       "positionClass": "toast-top-left"
                     });
                  @endforeach
                  </script>
                  @endif
                  <form action="{{ URL('/subscribe/store') }}"
                  method="post" enctype="multipart/form-data" id="formSubs">
                  {{ csrf_field() }}
                
                  <p>Notificarme cuando el sitio web esté listo.</p>
                  <div id="input-email">
                    <input id="textEmail" type="text" name="email" placeholder="Ingresa tu Correo">
                  </div>
                  <input id="submit" type="submit" value="Enviar">
                  {!! RecaptchaV3::field('contacto') !!}
                  </form>
                  <div class="iconsocial">
                    <a href="https://www.facebook.com/sycmarketingdigital/"><img class="social" src="img/facebook-logo.png" alt="Icono facebook"></a>
                    <a href="https://www.twitter.com/SCMarketingHN/"><img class="social" src="img/twitter.png" alt="Icono twitter"></a>
                    <a href="https://www.instagram.com/sycmarketingdigitalhn/"><img class="social" src="img/instagram.png" alt="Icono instagram"></a>
                  </div>
                </div>
            </div>
        </div>
        <!-- Second Half -->
        <div class="col-md-6 p-0 bg h-md-100">
            <div class="d-md-flex align-items-center h-md-100 p-5"></div>
        </div>
      </div>

      <script type="text/javascript">
        var countDownDate = new Date("Apr 29, 2019 00:00:00").getTime();
        var x = setInterval(function () {
            var now = new Date().getTime();
            var distance = countDownDate - now;
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / ( 1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            document.getElementById("launch").innerHTML = days + "d " + hours + "h " + minutes + "m " +
            seconds + "s ";

            if (distance < 0) {
              clearInterval(x);
              document.getElementById("launch").innerHTML = "Expired";
            };

        }, 1000);
      </script>
  </body>
</html>
