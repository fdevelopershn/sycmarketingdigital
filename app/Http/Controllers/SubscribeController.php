<?php

namespace marketing_digital\Http\Controllers;
use marketing_digital\Models\Subscribe;

use Illuminate\Http\Request;

class SubscribeController extends Controller
{
    public function ShowSubscribe()
    {
  
        return view('comingsoon');
      
    }
  
    public function StoreSubscribe(Request $request)
    {
      $subscribe = new Subscribe;
  
      $this->validate($request,[
        'email' => 'required|email|unique:subscribe',
        'g-recaptcha-response' => 'required|recaptchav3:contacto,0.5',
      ]);
  
      $subscribe->email = $request->input('email');
     
      $subscribe->save();
  
        return redirect(action('SubscribeController@ShowSubscribe'))
        ->with('action','Gracias por subscribirse');
      
    }
}
